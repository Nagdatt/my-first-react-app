import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import { Alert, AlertTitle } from '@material-ui/lab';
import AppleIcon from '@material-ui/icons/Apple';

import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    margin:5,
    
  },
  media: {
    height: 140,
  },
});

/*function Alerts(){
    return (
        <Alert severity="info" style={{margin:5,}}>
        <AlertTitle>Hello There</AlertTitle>
This is Demo React Page by— <strong>@Nagdatt</strong>
      </Alert>
    )
}*/
const handleClickVariant = (variant) => () => {
  // variant could be success, error, warning, info, or default
  //enqueueSnackbar('This is a success message!', { variant });
  console.log("Hello")
};

export default function MediaCard() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.root}> 
      <CardActionArea  onClick={handleClickVariant('info')}>
        <CardMedia
          className={classes.media}
          src="../../public/favicon.ico"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Lizard
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
            across all continents except Antarctica
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary"  >
          Share
        </Button>
        <Button size="small" color="primary" >
          Learn More
        </Button>
      </CardActions>
    </Card>

  );
}